//REGION 1
const gNAME_COL = ["orderCode", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
const gORDERCODE_COL = 0;
const gKICHCO_COL = 1;
const gLOAIPIZZA_COL = 2;
const gID_LOAINUOCUONG_COL = 3;
const gTHANHTIEN_COL = 4;
const gHOTEN_COL = 5;
const gSODIENTHOAI_COL = 6;
const gTRANGTHAI_COL = 7;
const gACTIONE_COL = 8;
var gOrderCode = 0;
var gIdOrder = 0;
var gTable_order = $("#table-order").DataTable({
    columns: [
        { data: gNAME_COL[gORDERCODE_COL] },
        { data: gNAME_COL[gKICHCO_COL] },
        { data: gNAME_COL[gLOAIPIZZA_COL] },
        { data: gNAME_COL[gID_LOAINUOCUONG_COL] },
        { data: gNAME_COL[gTHANHTIEN_COL] },
        { data: gNAME_COL[gHOTEN_COL] },
        { data: gNAME_COL[gSODIENTHOAI_COL] },
        { data: gNAME_COL[gTRANGTHAI_COL] },
        { data: gNAME_COL[gACTIONE_COL] }
    ],
    columnDefs: [
        {
            targets: gACTIONE_COL,
            defaultContent: `
            <div style="display: flex; gap: 10px;">
                <button class="edit-order btn btn-info"><i class="fas fa-pencil-alt"></i> Sửa</button>
                <button class="delete-order btn btn-danger"><i class="far fa-trash-alt"></i> Xóa</button>
            </div>
            `
        }
    ]
})
//REGION 2
onPageLoading();
//gán sự kiện cho nút lọc
$("#btn-filter").on('click', function () {
    onBtnFilterClick()
})
//gán sự kiện nút thêm
$("#btn-them").on('click', function () {
    onBtnAddClick()
})
//gán sự kiện nút thêm order trên modal
$("#modal-btn-them").click(function () {
    onBtnThemOrderClick();
})
//gán sự kiện nút edit
$("#table-order").on('click', '.edit-order', function () {
    onBtnEditClick(this)
})
//gán sự kiện nút xác nhận trên modal edit
$("#modal-btn-confirm-edit").on('click', function(){
    onBtnConfirmEditClick();
})
//gán sự kiện nut delete
$("#table-order").on('click', '.delete-order', function () {
    onBtnDeleteClick(this);
})
//gán sự kiện nút but xác nhận xóa trên modal delete
$("#modal-btn-confirm-delete").on('click', function(){
    onBtnConfirmDeleteClick();
})
//REGION 3
function onPageLoading() {
    getAllOrder();
}
//hàm thự hiện sự kiện nút btn filter được click
function onBtnFilterClick() {
    //b1 chuẩn bị dữ liệu, thu thập dữ liệu
    var vFilterOrder = {
        loaiPizza: "",
        trangThai: ""
    };
    //thu thập dữ liệu
    vFilterOrder.loaiPizza = $("#select-loai-pizza").val();
    vFilterOrder.trangThai = $("#select-trang-thai").val();
    //b2 tiến hành lọc dữ liệu
    filterOrder(vFilterOrder);
}
//hàm thực hiện sự kiện nút thêm click
function onBtnAddClick() {
    // Gán sự kiện change cho phần tử select kích cỡ
    $("#modal-select-combo").change(function () {
        var selectedSize = $(this).val();

        // Kiểm tra kích cỡ được chọn và điền thông tin tương ứng
        if (selectedSize === "S") {
            $("#modal-inp-duong-kinh").val("20");
            $("#modal-inp-suon").val("2");
            $("#modal-inp-salad").val("200");
            $("#modal-inp-so-luong-nuoc").val("2");
            $("#modal-inp-thanh-tien").val("150000");
        } else if (selectedSize === "M") {
            $("#modal-inp-duong-kinh").val("25");
            $("#modal-inp-suon").val("4");
            $("#modal-inp-salad").val("300");
            $("#modal-inp-so-luong-nuoc").val("3");
            $("#modal-inp-thanh-tien").val("200000");
        } else if (selectedSize === "L") {
            $("#modal-inp-duong-kinh").val("30");
            $("#modal-inp-suon").val("8");
            $("#modal-inp-salad").val("500");
            $("#modal-inp-so-luong-nuoc").val("4");
            $("#modal-inp-thanh-tien").val("250000");
        }
    });
    //b4 hiển thị thông tin
    $("#modal-them").modal("show");
    //load dato to select drink
    getDrinkList();
}
//hàm sử lý sự kiện nút thêm order được click
function onBtnThemOrderClick() {
    // b1 chuẩn bị dữ liệu, thu thập dữ liệu
    var vNewOrder = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: ""
    }
    //thu thập dữ liệu
    getDataAddOrderModal(vNewOrder);
    // b2 kiểm tra dữ liệu
    var vIsCheckNewOrder = validateNewOrder(vNewOrder);
    if (vIsCheckNewOrder) {
        // b3 gọi api
        createOrder(vNewOrder);
        // b4 thông báo hiển thị
        notificationNewOrder();
    }
}
//hàm sư lý sự kiện nút edit click
function onBtnEditClick(paramBtn) {
    //hàm lấy ordercode khi nút được ấn
    getDataBtnEditTable(paramBtn);
    //gọi api lấy order thông qua orderCode
    getOrderByOrderCode();
    $("#modal-update-trangthai").modal('show');
}
//hàm sử lý sự kiện nút btn confirm edit được click
function onBtnConfirmEditClick(){
    var vTrangThai = {
        trangThai: "",
    }
    vTrangThai.trangThai = $("#modal-select-update").val();
    //hàm gọi api đẻ update trạng thái
    $.ajax({
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders" + "/" + gIdOrder,
        type: "PUT",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(vTrangThai),
        success: function(response){
            console.log(response);
            notificationUpdateOrder();
        },
        error: function(err){
            console.log(err);
        }
    })
}
//hàm sử lý sự kiện nút delete được click
function onBtnDeleteClick(paramBtn) {
    getDataBtnDeleteTable(paramBtn);
    $("#modal-delete-order").modal('show');
}
//hàm sử lý sự kiện nút xác nhận xóa trên modal delete
function onBtnConfirmDeleteClick(){
    $.ajax({
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders" + "/" + gIdOrder,
        type: "DELETE",
        success: function(response){
            console.log(response);
            alert("Xóa Order thành công");
            onPageLoading();
            $("#modal-delete-order").modal('hide');
        },
        error: function(err){
            console.log(err.status);
        }
    })
}
//REGION 4
//hàm gọi api để lấy danh sách order
function getAllOrder() {
    $.ajax({
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
        type: "GET",
        success: function (response) {;
            console.log(response);
            loadDataToTable(response);
        },
        error: function (err) {
            console.log(err.status);
        }
    })
}
//hàm load data vào table
function loadDataToTable(paramRes) {
    gTable_order.clear();
    gTable_order.rows.add(paramRes);
    gTable_order.draw();
}
//hàm tiến hành lọc dữ liêu
function filterOrder(paramFilter) {
    var vLoaiPizza = paramFilter.loaiPizza;
    var vTrangThai = paramFilter.trangThai;
    //lọc và tìm kiếm dữ liệu trong DataTable
    gTable_order.search(vLoaiPizza + " " + vTrangThai).draw();
}
//hàm thu thập dữ liệu thêm order trên modal
function getDataAddOrderModal(paramOrder) {
    paramOrder.kichCo = $("#modal-select-combo").val();
    paramOrder.duongKinh = $("#modal-inp-duong-kinh").val()
    paramOrder.suon = $("#modal-inp-suon").val();
    paramOrder.salad = $("#modal-inp-salad").val();
    paramOrder.loaiPizza = $("#modal-select-loaipizza").val();
    paramOrder.idVourcher = $("#modal-inp-voucher").val().trim();
    paramOrder.idLoaiNuocUong = $("#modal-select-drink").val();
    paramOrder.soLuongNuoc = $("#modal-inp-so-luong-nuoc").val();
    paramOrder.hoTen = $("#modal-inp-name").val().trim();
    paramOrder.thanhTien = $("#modal-inp-thanh-tien").val();
    paramOrder.email = $("#modal-inp-email").val().trim();
    paramOrder.soDienThoai = $("#modal-inp-phone").val().trim();
    paramOrder.diaChi = $("#modal-inp-dia-chi").val().trim();
    paramOrder.loiNhan = $("#modal-inp-mesage").val().trim();
}
//hàm kiểm tra thêm order
function validateNewOrder(paramOrder) {
    if (paramOrder.kichCo == 0) {
        alert("Bạn chưa chọn combo");
        return false;
    }
    if (paramOrder.loaiPizza == 0) {
        alert("Bạn chưa chọn loại pizza");
        return false;
    }
    if (paramOrder.idLoaiNuocUong == 0) {
        alert("Bạn chưa chọn loại nước uống");
        return false;
    }
    if (paramOrder.hoTen == "") {
        alert("Bạn chưa nhập họ tên");
        return false;
    }
    if (paramOrder.soDienThoai == "") {
        alert("Bạn chưa nhập số điện thoại");
        return false;
    }
    if (paramOrder.trangThai == 0) {
        alert("Bạn chưa chọn trạng thái");
        return false;
    }
    if (paramOrder.diaChi == "") {
        alert("Bạn chưa nhập địa chỉ");
        return false;
    }
    if(paramOrder.email){
        // Biểu thức chính quy kiểm tra định dạng email
        var vEmailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
        // Kiểm tra xem email có khớp với biểu thức chính quy không
        if (vEmailRegex.test(paramOrder.email)) {
            return true; // Email hợp lệ
        } else {
            alert("Email không hợp lệ");
            return false; // Email không hợp lệ
        }
    }
    return true;
}
// Hàm gọi API để thêm một order
function createOrder(paramOrder) {
    $.ajax({
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders",
        type: "POST",
        contentType: "application/json;charset=UTF-8",
        data: JSON.stringify(paramOrder),
        success: function (newOrder) {
            console.log(newOrder);
        },
        error: function (err) {
            console.log(err.status);
        }
    });
}
//hàm thông báo đã thêm mới order thành công
function notificationNewOrder() {
    alert("Đã thêm Order thành công");
    onPageLoading();
    $("#modal-them").modal('hide');
}
//hàm lấy dữ liệu khi nút edit được ấn
function getDataBtnEditTable(paramBtn){
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramBtn).parents("tr");
    //Lấy datatable row
    var vDatatableRow = gTable_order.row(vRowSelected);
    //Lấy data của dòng 
    var vOrderData = vDatatableRow.data();
    var vOrderCode = vOrderData.orderCode;
    gOrderCode = vOrderCode;
}
//hàm lấy dữ liệu khi nút delete được ấn
function getDataBtnDeleteTable(paramBtn){
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramBtn).parents("tr");
    //Lấy datatable row
    var vDatatableRow = gTable_order.row(vRowSelected);
    //Lấy data của dòng 
    var vOrderData = vDatatableRow.data();
    var vIdOrder = vOrderData.id;
    gIdOrder = vIdOrder;
}
//hàm gọi api để lấy order thông qua ordercode
function getOrderByOrderCode(){
    $.ajax({
        url: "http://203.171.20.210:8080/devcamp-pizza365/orders" + "/" + gOrderCode,
        type: "GET",
        success: function(response){
            console.log(response);
            gIdOrder = response.id;
        },
        error: function(err){
            console.log(err.status);
        }
    })
}
//hàm hiển thị thông báo cập nhật trạng thái thành công
function notificationUpdateOrder(){
    alert("Cập nhật trạng thái thành công");
    onPageLoading();
    $("#modal-update-trangthai").modal('hide');
}
//hàm gọi api delete order
function getDrinkList(){
    $.ajax({
        url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
        type: "GET",
        success: function(response){
            console.log(response);
            loadDataDrinkToSelect(response);
        },
        error: function(err){
            console.log(err.status);
        }
    })
}
//hàm load data Drink to select drink
function loadDataDrinkToSelect(paramDrink){
    const selectElement = $("#modal-select-drink");

    // xóa các option trước
    selectElement.empty();

    // thêm option
    selectElement.append($("<option>", {
        value: 0,
        text: "--chọn--"
    }));

    // truyền dữ liệu vào option
    for (let bI = 0; bI < paramDrink.length; bI++) {
        const drink = paramDrink[bI];
        selectElement.append($("<option>", {
            value: drink.maNuocUong,
            text: drink.tenNuocUong
        }));
    }
}

